const mongoose = require("mongoose");

const { Schema } = mongoose;

const productSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  imageUrl: {
    type: String,
    required: true,
  },
  //productQuantity: { type: Number, default: 1 },
  buyPrice: {
    type: Number,
    required: true,
  },
  promotionPrice: {
    type: Number,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  timeCreated: {
    type: Date,
    default: Date.now,
    required: true,
  },
  timeUpdated: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

const ProductModel = mongoose.model("Product", productSchema);

module.exports = { ProductModel };
