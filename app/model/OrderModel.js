const mongoose = require("mongoose");

const { Schema } = mongoose;

const orderSchema = new Schema({
  _id: Schema.Types.ObjectId,
  customer: {
    type: String,
    //required: true,
  },
  orderDate: {
    type: Date,
    default: Date.now,
    //required: true,
  },
  requiredDate: {
    type: Date,
    default: Date.now,
    //required: true,
  },
  shippedDate: {
    type: Date,
    default: Date.now,
    //required: true,
  },
  note: {
    type: String,
    //default: null,
  },
  status: {
    type: Number,
    default: 0,
  },
  timeCreated: {
    type: Date,
    default: Date.now,
    //required: true,
  },
  timeUpdated: {
    type: Date,
    default: Date.now,
    //required: true,
  },
  orderDetails: [
    {
      type: Schema.Types.ObjectId,
      ref: "OrderDetail",
    },
  ],
});
const OrderModel = mongoose.model("Order", orderSchema);
module.exports = { OrderModel };
