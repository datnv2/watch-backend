const mongoose = require("mongoose");

const { Schema } = mongoose;

const orderDetailSchema = new Schema({
  _id: Schema.Types.ObjectId,
  order: {
    type: Schema.Types.ObjectId,
    //ref: "Order",
  },
  productId: {
    type: Schema.Types.ObjectId,
    //ref: "Product",
  },
  quantity: {
    type: Number,
    required: true,
  },
  priceEach: {
    type: Number,
    required: true,
  },
});
const OrderDetailModel = mongoose.model("OrderDetail", orderDetailSchema);
module.exports = { OrderDetailModel };
