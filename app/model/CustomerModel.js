const mongoose = require("mongoose");

const { Schema } = mongoose;

const customerSchema = new Schema({
  _id: Schema.Types.ObjectId,
  fullName: {
    type: String,
    required: true,
  },
  phoneNumber: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  // password: {
  //   type: String,
  //   required: true,
  // },
  address: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  timeCreated: {
    type: Date,
    default: Date.now,
    required: true,
  },
  timeUpdated: {
    type: Date,
    default: Date.now,
    required: true,
  },
  //   orders: [
  //     {
  //       type: Schema.Types.ObjectId,
  //       ref: "Order",
  //     },
  //   ],
});
const CustomerModel = mongoose.model("Customer ", customerSchema);
module.exports = { CustomerModel };
