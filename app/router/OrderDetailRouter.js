const express = require("express");
const {
  getAllOrderDetail,
  getOneOrderDetail,
  updateOrderDetail,
  deleteOrderDetail,
} = require("../controller/OrderDetailController");

const router = express.Router();

router.get("/", getAllOrderDetail);
router.get("/:orderDetailId", getOneOrderDetail);
router.put("/orderDetails/:orderDetailId", updateOrderDetail);
router.delete("/orderDetails/:orderDetailId", deleteOrderDetail);

module.exports = router;
