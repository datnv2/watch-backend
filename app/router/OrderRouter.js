const express = require("express");

const {
  createOrder,
  getAllOrder,
  getOneOrder,
  updateOrder,
  deleteOrder,
} = require("../controller/OrderController");

const {
  createOrderDetail,
  getAllOrderDetailOfOrder,
} = require("../controller/OrderDetailController");

const router = express.Router();

router.post("/", createOrder);
router.get("/", getAllOrder);
router.get("/:orderId", getOneOrder);
router.put("/:orderId", updateOrder);
router.delete("/:orderId", deleteOrder);
router.post("/:orderId/orderDetails", createOrderDetail);
router.get("/:orderId/orderDetails", getAllOrderDetailOfOrder);

module.exports = router;
