const express = require("express");

const {
  createCustomer,
  getAllCustomer,
  getOneCustomer,
  updateCustomer,
  deleteCustomer,
  loginCustomerByEmail,
  loginCustomerEmailOrPhone,
} = require("../controller/CustomerController");

const router = express.Router();

router.post("/", createCustomer);
router.get("/", getAllCustomer);
router.get("/:customerId", getOneCustomer);
router.put("/:customerId", updateCustomer);
router.delete("/:customerId", deleteCustomer);
router.get("/customer-login", loginCustomerByEmail);
router.get("/customers-login", loginCustomerEmailOrPhone);

module.exports = router;
