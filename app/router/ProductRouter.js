const express = require("express");

const {
  CreateProduct,
  GetAllProduct,
  getProductPage,
  GetProductByID,
  UpdateProduct,
  DeleteProduct,
} = require("../controller/ProductController");

const router = express.Router();

router.post("/", CreateProduct);
router.get("/", GetAllProduct);
router.get("/all-products", GetAllProduct);
router.get("/:productId", GetProductByID);
router.put("/:productId", UpdateProduct);
router.delete("/:productId", DeleteProduct);

module.exports = router;
