const mongoose = require("mongoose");
const { OrderModel } = require("../model/OrderModel");

// hàm tạo mới order
function createOrder(request, response) {
  const order = new OrderModel({
    _id: mongoose.Types.ObjectId(),
    customer: request.body.customer,
    orderDate: request.body.orderDate,
    requiredDate: request.body.requiredDate,
    shippedDate: request.body.shippedDate,
    note: request.body.note,
    status: request.body.status,
    timeCreated: request.body.timeCreated,
    timeUpdated: request.body.timeUpdated,
  });

  order
    .save()
    .then((newOrder) => {
      return response.status(200).json({
        message: "Success",
        order: newOrder,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm lấy ra toàn bộ order
function getAllOrder(request, response) {
  const condition = {};

  if (request.query.customerId) {
    condition.customerId = request.query.customerId;
  }

  OrderModel.find(condition)
    .select(
      "_id customerId orderDate requiredDate shippedDate note status timeCreated timeUpdated "
    )
    .then((orderList) => {
      return response.status(200).json({
        message: "Success",
        orders: orderList,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm này lấy ra order dựa vào id
function getOneOrder(request, response) {
  const orderId = request.params.orderId;
  if (mongoose.Types.ObjectId.isValid(orderId)) {
    OrderModel.findById(orderId)
      .then((data) => {
        if (data) {
          return response.status(200).json({
            message: "Success",
            order: data,
          });
        } else {
          return response.status(404).json({
            message: "Fail",
            order: "Not found",
          });
        }
      })

      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "OrderID is not valid",
    });
  }
}

// hàm này thực hiện update order theo id
function updateOrder(request, response) {
  const orderId = request.params.orderId;

  const updateObject = request.body;

  if (mongoose.Types.ObjectId.isValid(orderId)) {
    OrderModel.findByIdAndUpdate(orderId, updateObject)
      .then((updatedOrder) => {
        return response.status(200).json({
          message: "success",
          updatedOrder: updatedOrder,
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "OrderID is not valid",
    });
  }
}

// hàm này thực hiện việc xóa order dựa vào id
function deleteOrder(request, response) {
  const orderId = request.params.orderId;
  if (mongoose.Types.ObjectId.isValid(orderId)) {
    OrderModel.findByIdAndDelete(orderId)
      .then(() => {
        return response.status(200).json({
          message: "Success",
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "OrderID is not valid",
    });
  }
}

module.exports = {
  createOrder,
  getAllOrder,
  getOneOrder,
  updateOrder,
  deleteOrder,
};
