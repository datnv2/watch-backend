const mongoose = require("mongoose");

const { CustomerModel } = require("../model/CustomerModel");

// hàm tạo mới customer
function createCustomer(request, response) {
  const customer = new CustomerModel({
    _id: mongoose.Types.ObjectId(),
    fullName: request.body.fullName,
    phoneNumber: request.body.phoneNumber,
    email: request.body.email,
    // password: request.body.password,
    address: request.body.address,
    city: request.body.city,
    country: request.body.country,
  });

  customer
    .save()
    .then((newCustomer) => {
      return response.status(200).json({
        message: "Success",
        customer: newCustomer,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm lấy ra toàn bộ customer
function getAllCustomer(request, response) {
  const condition = {};

  if (request.query.fullName) {
    condition.fullName = request.query.fullName;
  }

  CustomerModel.find(condition)
    .select("_id fullName phoneNumber email  address city country")
    .then((customerList) => {
      return response.status(200).json({
        message: "Success",
        customers: customerList,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm này lấy ra customer dựa vào id
function getOneCustomer(request, response) {
  const customerId = request.params.customerId;
  if (mongoose.Types.ObjectId.isValid(customerId)) {
    CustomerModel.findById(customerId)
      .then((data) => {
        if (data) {
          return response.status(200).json({
            message: "Success",
            customer: data,
          });
        } else {
          return response.status(404).json({
            message: "Fail",
            customer: "Not found",
          });
        }
      })

      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "CustomerID is not valid",
    });
  }
}

// hàm này thực hiện update customer theo id
function updateCustomer(request, response) {
  const customerId = request.params.customerId;

  const updateObject = request.body;

  if (mongoose.Types.ObjectId.isValid(customerId)) {
    CustomerModel.findByIdAndUpdate(customerId, updateObject)
      .then((updatedCustomer) => {
        return response.status(200).json({
          message: "success",
          updatedCustomer: updatedCustomer,
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "CustomerID is not valid",
    });
  }
}

// hàm này thực hiện việc xóa customer dựa vào id
function deleteCustomer(request, response) {
  const customerId = request.params.customerId;

  if (mongoose.Types.ObjectId.isValid(customerId)) {
    CustomerModel.findByIdAndDelete(customerId)
      .then(() => {
        return response.status(200).json({
          message: "Success",
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "CustomerID is not valid",
    });
  }
}

// hàm đăng nhập email + password
const loginCustomerByEmail = async (req, res) => {
  try {
    const email = req.query.email;
    const password = req.query.password;
    const customer = await CustomerModel.findOne({
      $and: [{ email: email }, { password: password }],
    });
    res.status(200).json({
      success: true,
      message: "login successfully",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "login failed",
      error: err.message,
    });
  }
};

// hàm đăng nhập bằng email hoặc số điện thoại
const loginCustomerEmailOrPhone = async (req, res) => {
  try {
    const login = req.query.login;
    const customer = await CustomerModel.findOne({
      $or: [
        { email: login },
        { phoneNumber: login } /* , { password: login } */,
      ],
    });
    res.status(200).json({
      success: true,
      message: "Success",
      customer: customer,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "login failed",
      error: err.message,
    });
  }
};

module.exports = {
  createCustomer,
  getAllCustomer,
  getOneCustomer,
  updateCustomer,
  deleteCustomer,
  loginCustomerByEmail,
  loginCustomerEmailOrPhone,
};
