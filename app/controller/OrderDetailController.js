const mongoose = require("mongoose");

const { OrderModel } = require("../model/OrderModel");

const { OrderDetailModel } = require("../model/OrderDetailModel");

// Create OrderDetail
function createOrderDetail(req, res) {
  const orderDetail = new OrderDetailModel({
    _id: mongoose.Types.ObjectId(),
    order: req.body.order,
    productId: req.body.productId,
    quantity: req.body.quantity,
    priceEach: req.body.priceEach,
  });
  orderDetail
    .save()
    .then(function (newOrderDetail) {
      var orderId = req.params.orderId;
      return OrderModel.findOneAndUpdate(
        { _id: orderId },
        { $push: { orderDetails: newOrderDetail._id } },
        { new: true }
      );
    })
    .then((updatedOrder) => {
      return res.status(200).json({
        success: true,
        message: "New OrderDetail created successfully on Order",
        Order: updatedOrder,
      });
    })
    .catch((error) => {
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
        error: error.message,
      });
    });
}

// Get all orderdetail of order
function getAllOrderDetailOfOrder(req, res) {
  const orderId = req.params.orderId;

  OrderModel.findById(orderId)

    .populate({ path: "orderDetails" })

    .then((singleOrder) => {
      res.status(200).json({
        success: true,
        message: `More orderDetails on ${singleOrder.customer}`,
        OrderDetails: singleOrder,
      });
    })

    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "This order does not exist",
        error: err.message,
      });
    });
}

// Get all review
function getAllOrderDetail(req, res) {
  OrderDetailModel.find()
    .select("_id order productId quantity priceEach")
    .then((allOrderDetail) => {
      return res.status(200).json({
        success: true,
        message: "A list of all order detail",
        OrderDetail: allOrderDetail,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
        error: err.message,
      });
    });
}

// hàm này để lấy ra order dựa vào id truyền vào
function getOneOrderDetail(req, res) {
  const id = req.params.orderDetailId;

  OrderDetailModel.findById(id)
    .then((singleOrderDetail) => {
      res.status(200).json({
        success: true,
        message: `Get data on OrderDetail`,
        OrderDetail: singleOrderDetail,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "This orderDetail does not exist",
        error: err.message,
      });
    });
}

// update orderdetail
function updateOrderDetail(req, res) {
  const orderDetailId = req.params.orderDetailId;
  const updateObject = req.body;
  OrderDetailModel.findByIdAndUpdate(orderDetailId, updateObject)
    .then((updatedOrderDetail) => {
      res.status(200).json({
        success: true,
        message: "OrderDetail is updated",
        updatedOrderDetail: updatedOrderDetail,
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: "Server error. Please try again.",
      });
    });
}

// delete a orderdetail
function deleteOrderDetail(req, res) {
  const id = req.params.orderDetailId;

  OrderDetailModel.findByIdAndRemove(id)
    .exec()
    .then(() =>
      res.status(200).json({
        success: true,
      })
    )
    .catch((err) =>
      res.status(500).json({
        success: false,
      })
    );
}
module.exports = {
  createOrderDetail,
  getAllOrderDetailOfOrder,
  getAllOrderDetail,
  getOneOrderDetail,
  updateOrderDetail,
  deleteOrderDetail,
};
