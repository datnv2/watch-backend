const mongoose = require("mongoose");
const { ProductModel } = require("../model/ProductModel");

// hàm tạo mới product
function CreateProduct(request, response) {
  const product = new ProductModel({
    _id: mongoose.Types.ObjectId(),
    name: request.body.name,
    type: request.body.type,
    imageUrl: request.body.imageUrl,
    //productQuantity: request.body.productQuantity,
    buyPrice: request.body.buyPrice,
    promotionPrice: request.body.promotionPrice,
    description: request.body.description,
  });

  product
    .save()
    .then((newProduct) => {
      return response.status(200).json({
        message: "Success",
        product: newProduct,
      });
    })
    .catch((error) => {
      return response.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}

// hàm lấy ra toàn bộ product
// function GetAllProduct(request, response) {
//   const condition = {};
//   if (request.query.name) {
//     condition.name = request.query.name;
//   }
//   ProductModel.find(condition)
//     .select("_id name type imageUrl buyPrice promotionPrice description")
//     .then((productList) => {
//       return response.status(200).json({
//         message: "Success",
//         products: productList,
//       });
//     })
//     .catch((error) => {
//       return response.status(500).json({
//         message: "Fail",
//         error: error.message,
//       });
//     });
// }

const GetAllProduct = async (req, res) => {
  // thực hiện phân trang và set số sản phẩm mỗi trang là 8
  const pageSize = 8;
  //const pageSize = Number(req.query.limit) || 16;
  // trang đầu tiên là 1
  const page = Number(req.query.pageNumber) || 1;

  const keyword = req.query.keyword
    ? { name: { $regex: req.query.keyword, $options: "i" } }
    : {};

  // set giá để tìm theo giá min, max của sản phẩm
  const maxPrice = req.query.maxPrice;
  const minPrice = req.query.minPrice;
  const priceFilter =
    maxPrice && minPrice
      ? { promotionPrice: { $gte: minPrice, $lte: maxPrice } }
      : {};

  // thực hiện tìm theo tên
  const count = await ProductModel.find({ ...priceFilter }).countDocuments({
    ...keyword,
  });

  // thực hiện tìm kiếm theo sản phẩm
  const products = await ProductModel.find({ ...keyword })
    .find({ ...priceFilter })
    .limit(pageSize)
    .skip(pageSize * (page - 1));

  // thực hiện để làm tròn số lượng sản phẩm trên mỗi trang.
  res.json(
    {
      success: true,
      Product: products,
      page,
      pages: Math.ceil(count / pageSize),
    },
    minPrice,
    maxPrice
  );
};

// phân trang
const getProductPage = async (req, res) => {
  const page = parseInt(req.query.page);
  const limit = 1;
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const results = {};
  if (endIndex < (await ProductModel.countDocuments().exec())) {
    results.next = {
      page: page + 1,
      limit: limit,
    };
  }
  if (startIndex > 0) {
    results.previous = {
      page: page - 1,
      limit: limit,
    };
  }
  try {
    const allProduct = await ProductModel.find();
    const pageNumber = allProduct.length / 8;
    const productList = await ProductModel.find().limit(limit).skip(startIndex);
    res.status(200).json({
      success: true,
      message: "product list",
      pageNum: pageNumber,
      ProductModel: productList,
    });
  } catch (err) {
    res.status(500).json({
      success: false,
      message: "get all failed",
      error: err.message,
    });
  }
};

// hàm này lấy ra product dựa vào id
function GetProductByID(request, response) {
  const productId = request.params.productId;
  if (mongoose.Types.ObjectId.isValid(productId)) {
    ProductModel.findById(productId)
      .then((data) => {
        if (data) {
          return response.status(200).json({
            message: "Success",
            product: data,
          });
        } else {
          return response.status(404).json({
            message: "Fail",
            product: "Not found",
          });
        }
      })

      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "ProductID is not valid",
    });
  }
}

// hàm này thực hiện update product theo id
function UpdateProduct(request, response) {
  const productId = request.params.productId;

  const updateObject = request.body;

  if (mongoose.Types.ObjectId.isValid(productId)) {
    ProductModel.findByIdAndUpdate(productId, updateObject)
      .then((updatedProduct) => {
        return response.status(200).json({
          message: "success",
          updatedProduct: updatedProduct,
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "ProductID is not valid",
    });
  }
}

// hàm này thực hiện việc xóa product dựa vào id
function DeleteProduct(request, response) {
  const productId = request.params.productId;

  if (mongoose.Types.ObjectId.isValid(productId)) {
    ProductModel.findByIdAndDelete(productId)
      .then(() => {
        return response.status(200).json({
          message: "Success",
        });
      })
      .catch((error) => {
        return response.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    return response.status(400).json({
      message: "Fail",
      error: "ProductID is not valid",
    });
  }
}
module.exports = {
  CreateProduct,
  GetAllProduct,
  getProductPage,
  GetProductByID,
  UpdateProduct,
  DeleteProduct,
};
