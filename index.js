const express = require("express");
const mongoose = require("mongoose");

const ProductRouter = require("./app/router/ProductRouter");
const CustomerRouter = require("./app/router/CustomerRouter");
const OrderRouter = require("./app/router/OrderRouter");
const OrderDetailRouter = require("./app/router/OrderDetailRouter");

const app = express();

// khai báo để body có thể truyền đc tiếng việt
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use(express.json());

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

const port = 8001;

async function connectMongoDB() {
  await mongoose.connect("mongodb://localhost:27017/Watch_Backend");
}

connectMongoDB()
  .then(() => console.log("Connect successfully!"))
  .catch((err) => console.log(err));

app.get("/", (request, response) => {
  response.json({
    message: "CRUD Shop24h Rest API",
  });
});

app.use("/products", ProductRouter);
app.use("/customers", CustomerRouter);
app.use("/orders", OrderRouter);
app.use("/orderDetails", OrderDetailRouter);

app.listen(port, () => {
  console.log(`CRUD app listening at http://localhost:${port}`);
});
